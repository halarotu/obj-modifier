import java.nio.FloatBuffer;
import java.util.Arrays;

public class ObjModifier {
    public static void centerVertices(FloatBuffer buf) {
        // We often get 3D models which are not positioned at the origin of the coordinate system. Placing such models
        // on a 3D map is harder for users. One solution is to reposition such a model so that its center is at the
        // origin of the coordinate system. Your task is to implement the centering.
        //
        // FloatBuffer stores the vertices of a mesh in a continuous array of floats (see below)
        // [x0, y0, z0, x1, y1, z1, ..., xn, yn, zn]
        // This kind of layout is common in low-level 3D graphics APIs.
        // TODO: Implement your solution here

        int capasity = buf.capacity();
        float[] xCoordidates = new float[capasity/3];
        float[] yCoordidates = new float[capasity/3];
        float[] zCoordidates = new float[capasity/3];


        for (int index = 0; index < capasity; index++) {
            switch (index % 3) {
                case 0:
                    xCoordidates[index/3] = buf.get(index);
                    break;

                case 1:
                    yCoordidates[index/3] = buf.get(index);
                    break;

                case 2:
                    zCoordidates[index/3] = buf.get(index);
                    break;
            }
        }
        float xAverage = average(xCoordidates);
        float yAverage = average(yCoordidates);
        float zAverage = average(zCoordidates);

        buf.rewind();
        for (int index = 0; index < capasity; index++) {
            switch (index % 3) {
                case 0:
                    buf.put(xCoordidates[index/3] - xAverage);
                    break;
                case 1:
                    buf.put(yCoordidates[index/3] - yAverage);
                    break;
                case 2:
                    buf.put(zCoordidates[index/3] - zAverage);
                    break;
            }
        }


    }


    private static float average(float[] array) {
        float sum = 0;
        for (float current : array) {
            sum += current;
        }
        return sum/array.length;
    }
}
